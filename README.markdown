# My fork of underwater-theme.el

Made a bunch of modifications over the months, check commit history if you're
curious.

You can find the original [here](https://github.com/jmdeldin/underwater-theme.el).
